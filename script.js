var intervalID = window.setInterval(updateScreen, 200);
var c = document.getElementById("console");

var txt = [
  "FORCE: XX0022. ENCYPTED://167.71.209.XXX",
  "TRYPASS: ********* AUTH CODE: ALPHA GAMMA: 1___ PRIORITY 1",
  "RETRY: REINDEER FLOTILLA",
  "Z:> /SCLL/UOC/GAME/TITANS/ EXECUTE -PLAYERS 1 2 3 4",
  "================================================",
  "Priority 1 // local / scanning...",
  "scanning ports...",
  "BACKDOOR FOUND (167.71.209.000)",
  "BACKDOOR FOUND (167.71.209.001)",
  "BACKDOOR FOUND (167.71.209.002)",
  "...",
  "...",
  "CLL.EXE -r -z",
  "...locating vulnerabilities...",
  "...vulnerabilities found...",
  "MCP/> DEPLOY SCLL",
  "SCAN: __ 167.71.209.001",
  "SCAN: __ 167.71.209.030",
  "SCAN: __ 167.71.209.065",
  "SCAN: __ 167.71.209.100",
  "SCAN: __ 167.71.209.110",
  "SCAN: __ 167.71.209.120",
  "DECRYPTED://167.71.209.133",
];

var docfrag = document.createDocumentFragment();

function updateScreen() {
  //Shuffle the "txt" array
  txt.push(txt.shift());
  //Rebuild document fragment
  txt.forEach(function (e) {
    var p = document.createElement("p");
    p.textContent = e;
    docfrag.appendChild(p);
  });
  //Clear DOM body
  while (c.firstChild) {
    c.removeChild(c.firstChild);
  }
  c.appendChild(docfrag);
}
